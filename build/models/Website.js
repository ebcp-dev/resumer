'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @module src/models/Website */

/** Define Website model as function. */
var WebsiteModel = exports.WebsiteModel = function WebsiteModel(sequelize, type) {
  return sequelize.define('website', {
    id: {
      type: type.UUID,
      primaryKey: true,
      defaultValue: type.UUIDV4
    },
    name: {
      type: type.STRING
    },
    url: {
      type: type.STRING
    },
    status: {
      type: type.STRING,
      defaultValue: 'Online'
    }
  });
};
//# sourceMappingURL=Website.js.map