'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Website = exports.User = exports.sequelize = undefined;

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _User = require('./models/User');

var _Website = require('./models/Website');

var _keys = require('./config/keys');

var _server = require('./server');

var _server2 = _interopRequireDefault(_server);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** Connect to database using config keys. */

/** Import model definitions and database credentials. */
var sequelize = exports.sequelize = new _sequelize2.default(_keys.db, _keys.dbuser, _keys.dbpass, {
  host: _keys.dbhost,
  dialect: 'postgres',
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  logging: false
});

/** Create models with imported definitions. */
/** @module src/sequelize */

/** Import sequelize dependency. */
var User = exports.User = (0, _User.UserModel)(sequelize, _sequelize2.default);
var Website = exports.Website = (0, _Website.WebsiteModel)(sequelize, _sequelize2.default);
/** User's primary key (id) will be Website's foreign key as 'userId'. */
Website.belongsTo(User, { foreignKey: 'userId' });

sequelize.sync().then(function () {
  console.log('Database & tables created!');
  /** Emit event once database has been created. */
  _server2.default.emit('Database ready.');
});
//# sourceMappingURL=sequelize.js.map