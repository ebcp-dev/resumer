'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _sequelize = require('../sequelize');

var _addWebsite = require('../validation/addWebsite');

var _addWebsite2 = _interopRequireDefault(_addWebsite);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @module src/routes/website */

/** Import dependencies. */
var router = _express2.default.Router();

/** Import defined Website schema from sequelize. */


/** Import validation function. */


/** Add website route */
router.post('/add', _passport2.default.authenticate('jwt', { session: false }), function (req, res) {
  var _validateWebsiteInput = (0, _addWebsite2.default)(req.body),
      errors = _validateWebsiteInput.errors,
      isValid = _validateWebsiteInput.isValid;

  if (!isValid) {
    return res.status(400).json(errors);
  }

  var newWebsite = {
    url: req.body.url,
    name: req.body.name,
    userId: req.user.id
  };

  _sequelize.Website.findOrCreate({
    where: { userId: newWebsite.userId, url: newWebsite.url },
    defaults: newWebsite
  }).spread(function (website, created) {
    if (!created) {
      errors.url = 'Website already added.';
      return res.status(400).json({ errors: errors, website: website });
    } else {
      return res.status(200).json(website);
    }
  });
});

/** Get list of websites of current authenticated user. */
router.get('/list', _passport2.default.authenticate('jwt', {
  session: false
}), function (req, res) {
  _sequelize.Website.findAll({
    where: { userId: req.user.id }
  }).then(function (websites) {
    return res.status(200).json(websites);
  });
});

exports.default = router;
//# sourceMappingURL=website.js.map