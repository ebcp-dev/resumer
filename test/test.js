import request from 'supertest';
import chai from 'chai';
const expect = chai.expect;
import app from '../src/server';
import { User, Website, sequelize } from '../src/sequelize';

// Test login credentials
const testInput = {
  test: {
    email: 'testemail@gmail.com',
    password: 'password'
  },
  wrongEmail: {
    email: 'testemai@gmail.com',
    password: 'password'
  },
  wrongPassword: {
    email: 'testemail@gmail.com',
    password: 'passwords'
  },
  testSignup: {
    email: 'testemail@gmail.com',
    password: 'password',
    password2: 'password'
  },
  signupUnconfirmedPasswords: {
    email: 'testemail@gmail.com',
    password: 'password',
    password2: 'password2'
  },
  emptyInput: {
    email: '',
    password: ''
  },
  testWebsite: {
    url: 'https://slack.com',
    name: 'Slack'
  },
  invalidWebsite: {
    url: 'https://slack',
    name: 'Slack'
  }
};
let token;

before(function(done) {
  this.timeout(10000);
  User.sync({ force: true }).then(res => {
    Website.destroy({ where: {}, truncate: true });
    request(app)
      .post('/api/user/signup')
      .send(testInput.testSignup)
      .end((err, res) => {
        request(app)
          .post('/api/user/login')
          .send(testInput.test)
          .end((err, res) => {
            token = res.body.session;
            done();
          });
      });
  });
});

describe('/user/login and /user/signup Authentication routes', done => {
  it('Status 200 on successful login', done => {
    request(app)
      .post('/api/user/login')
      .send(testInput.test)
      .expect(200)
      .end((err, res) => {
        token = res.body.session;
        expect(res.body.success).to.be.true;
        done();
      });
  });
  it('Status 400 on wrong login email', done => {
    request(app)
      .post('/api/user/login')
      .send(testInput.wrongEmail)
      .expect(400, done);
  });
  it('Status 400 on wrong login password', done => {
    request(app)
      .post('/api/user/login')
      .send(testInput.wrongPassword)
      .expect(400, done);
  });
  it('Status 400 on empty login input', done => {
    request(app)
      .post('/api/user/login')
      .send(testInput.emptyInput)
      .expect(400, done);
  });
  it('Status 400 on existing signup email', done => {
    request(app)
      .post('/api/user/signup')
      .send(testInput.testSignup)
      .expect(400, done);
  });
  it('Status 400 on empty signup input', done => {
    request(app)
      .post('/api/user/signup')
      .send(testInput.emptyInput)
      .expect(400, done);
  });
  it('Status 400 on confirm password signup failure', done => {
    request(app)
      .post('/api/user/signup')
      .send(testInput.signupUnconfirmedPasswords)
      .expect(400, done);
  });
  it('Status 200 on authorized access for /current', done => {
    request(app)
      .get('/api/user/current')
      .set('Authorization', token)
      .expect(200, done);
  });
  it('Status 401 on unauthorized access for /current', done => {
    request(app)
      .get('/api/user/current')
      .expect(401, done);
  });
});

describe('GET /website/list route before adding', done => {
  it('Status 200 with empty array when no data is saved', done => {
    request(app)
      .get('/api/website/list')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        expect(res.body.length).equal(0);
        done();
      });
  });

  it('Status 401 on unauthorized access', done => {
    request(app)
      .get('/api/website/list')
      .expect(401, done);
  });
});

describe('POST /website/add routes', done => {
  it('Status 200 on successful add', done => {
    request(app)
      .post('/api/website/add')
      .set('Authorization', token)
      .send(testInput.testWebsite)
      .expect(200)
      .end((err, res) => {
        expect(res.body.name).equal(testInput.testWebsite.name);
        done();
      });
  });
  it('Status 400 on empty input', done => {
    request(app)
      .post('/api/website/add')
      .set('Authorization', token)
      .send(testInput.emptyInput)
      .expect(400, done);
  });
  it('Status 400 on invalid url', done => {
    request(app)
      .post('/api/website/add')
      .set('Authorization', token)
      .send(testInput.invalidWebsite)
      .expect(400, done);
  });
  it('Status 400 on already existing website', done => {
    request(app)
      .post('/api/website/add')
      .set('Authorization', token)
      .send(testInput.testWebsite)
      .expect(400)
      .end((err, res) => {
        expect(res.body.errors.url).equal('Website already added.');
        done();
      });
  });
  it('Status 401 on unauthorized access', done => {
    request(app)
      .post('/api/website/add')
      .send(testInput.testWebsite)
      .expect(401, done);
  });
});

describe('GET /website/list route with data', done => {
  it('Status 200 with list of added websites', done => {
    request(app)
      .get('/api/website/list')
      .set('Authorization', token)
      .expect(200)
      .end((err, res) => {
        expect(res.body.length).equal(1);
        done();
      });
  });

  it('Status 401 on unauthorized access', done => {
    request(app)
      .get('/api/website/list')
      .expect(401, done);
  });
});

after(done => {
  // Empty database
  Website.destroy({ where: {}, truncate: true });
  User.destroy({ where: {}, truncate: true });
  sequelize.close();
  done();
  // Need to manually exit mocha
  process.exit(0);
});
