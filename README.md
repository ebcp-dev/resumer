# An API written in Node with a PostgreSQL database and JWT authentication.

[![Build Status](https://travis-ci.org/ebcp-dev/resumer.svg?branch=master)](https://travis-ci.org/ebcp-dev/resumer)

Mocha and Chai are used for unit tests. NYC is used for code coverage reporting. Integrated with Travis CI.
