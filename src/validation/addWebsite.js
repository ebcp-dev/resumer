/** @module src/validation/addWebsite */

/** Import Validator dependency. */
import Validator from 'validator';
/** Import isEmpty custom function*/
import isEmpty from './utility/is-empty';

/**
 * Function to validate arguments.
 * Checks if arguments are empty or invalid and returns an error object
 * and a boolean value whether the error object is empty or not.
 */
const validateWebsiteInput = data => {
  /** Define errors object. */
  let errors = {};

  /** Replace values to empty string if object key is empty. */
  data.name = !isEmpty(data.name) ? data.name : '';
  data.url = !isEmpty(data.url) ? data.url : '';

  /** Set name value to required error message if empty. */
  if (Validator.isEmpty(data.name)) {
    errors.name = 'Name is required.';
  }

  /** Set url value to required error message if empty. */
  if (Validator.isEmpty(data.url)) {
    errors.url = 'URL is required.';
  }

  /** Return error if invalid URL. */
  if (!Validator.isURL(data.url)) {
    errors.url = 'URL is invalid.';
  }

  /** Return errors object and isValid boolean value. */
  return {
    errors,
    isValid: isEmpty(errors)
  };
};

export default validateWebsiteInput;
