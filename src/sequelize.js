/** @module src/sequelize */

/** Import sequelize dependency. */
import Sequelize from 'sequelize';
/** Import model definitions and database credentials. */
import { UserModel } from './models/User';
import { WebsiteModel } from './models/Website';
import { db, dbuser, dbpass, dbhost } from './config/keys';
import app from './server';
/** Connect to database using config keys. */
export const sequelize = new Sequelize(db, dbuser, dbpass, {
  host: dbhost,
  dialect: 'postgres',
  dialectOptions: {
    ssl: true
  },
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  logging: false
});

/** Create models with imported definitions. */
export const User = UserModel(sequelize, Sequelize);
export const Website = WebsiteModel(sequelize, Sequelize);
/** User's primary key (id) will be Website's foreign key as 'userId'. */
Website.belongsTo(User, { foreignKey: 'userId' });

sequelize.sync().then(() => {
  console.log(`Database & tables created!`);
  /** Emit event once database has been created. */
  app.emit('Database ready.');
});
